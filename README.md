# Erebo

Erebo is a system that allows provisioning VPN from several providers and offering those VPNs as a service allowing connection to these as if you were using a proxy

## Server

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Python version supported

The Python version supported is the 3.6 and 3.7

### Prerequisites

* The software requires [Docker](https://www.docker.com/) installed in the base operating system.
* The software requires a user with permission over docker and iptables
* The software requires a Redis Server.
* The Python requirements are in the file requirements.txt, the installation of the program using pip satisfies the necessary dependencies.
* The software requires the ovpn files and one file per user and password delivered by the vpn provider

### Installing

Install [Docker](https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/debian) in the operating system

To install Erebo run:

```bash
pip install erebo
```

### Config

The system requires this files:

* [main.ini](#main): File with the config to use Redis and directory where copy ovpn file
* [files_auth.json](#files_auth): File in json format with configuration per user and vpn provider
* [file_name_auth.txt](#file_name_auth): : unique name per vpn provider

#### main

The main.ini files is like this:

* host: Redis server IP
* passw: password to connect a redis or ''
* port: Redis server IP
* dbname: Redis database number
* channel: channel used to get vpn requests
* channel_response: channel used to get vpn respond
* ovpn_root_dir: /opt/vpn
* user: user (currently the same user used to start the software)
* passw: user password 

[Example](https://gitlab.com/lennin.caro/erebo/blob/master/main.ini)

```python
[Redis]
host = 127.0.0.1
passw = password
port = 6379
dbname = 4
channel = vpn
channel_response = vpn_response
[Files]
ovpn_root_dir = /opt/vpn
[Sudo]
user = user_name
passw = password
```

#### files_auth

The files_auth.json files is like this:

* node_name: Identification key, usualy is the provider name
* directory: OVPN files ubication, This location must be accessible by the software
* container_dirpath: OVPN ubication inside container, recomend use "/tmp"
* regex_location: a list of regex condition to extract the country using ovpn file name. The regex have to return [ISO] 2, [ISO] 3 or [ISO] 3166 in the group 1. Example:
    * ovpn file name "us3823.nordvpn.com.tcp.ovpn"
    * regext to get country "(\^[A-Za-z]{2})"
    * return "us"
* file_name_auth.txt: unique name by vpn provider
* max_connections: maximum simultaneous connections allowed for the user saved in "file_name_auth.txt"
* used_connections: number of connection used, this value is stored in Redis

File content:

```json
{
    "node_name": {
        "directory": ,
        "container_dirpath": "/tmp",
        "regex_location": [],
        "files_auth": {
            "file_name_auth.txt": {
                "max_connections": ,
                "used_connections": 0
            }
        }
    }
}
```

[Example](https://gitlab.com/lennin.caro/erebo/blob/master/files_auth.json)

```json
{
    "nordvpn": {
        "directory": "/opt/vpn/nordvpn",
        "container_dirpath": "/tmp",
        "regex_location": [
            "(^[A-Za-z]{2})"
        ],
        "files_auth": {
            "auth_01.txt": {
                "max_connections": 5,
                "used_connections": 0
            }
        }
    },
    "express_vpn": {
        "directory": "/opt/vpn/express",
        "container_dirpath": "/tmp",
        "regex_location": [
            "my_expressvpn_(\\w+)_"
        ],
        "files_auth": {
            "auth_01.txt": {
                "max_connections": 5,
                "used_connections": 0
            },
            "auth_02.txt": {
                "max_connections": 5,
                "used_connections": 0
            }
        }
    },
}
```

### file_name_auth

File with unique name by vpn provider, this file must be located in the same directory of the ovpn files, this path is configured in the directory key under node_name

This file where have two line:

* line one: user
* line two: password

[Example](https://gitlab.com/lennin.caro/erebo/blob/master/file_name_auth.txt):


```
user_name
password
```

### Start Server

To be able to start erebo you must run:

```bash
erebo config_file_location vpn_config_location
```

**config_file_location** is the [main.ini](#main) file
**config_file_location** is the [files_auth.json](#files_auth) file

## Architecture Diagram

![Diagram](erebo.png)

## Client

You can use the [Redis](https://redislabs.com/) client directly from the command line or you can use a redis SDK depending on the programming language, in python is this [Redis SDK](https://pypi.org/project/redis/).

### Redis version supported

The Redis client version supported is the 4.0.9 or higher

### Redis message

The software use Redis pub/sub mechanism, to use this we need a channel and a message, the message is a json format.

```redis
PUBLISH [channel_name] [message]
```

#### Mesagge format

The message is a json, the format is:

```json
{
    "task": "command",
    "name": "container_name",
    "iso2": "country iso2 [opcional]"
}

```

* **task**: one of the following commands.

    * create: create a vpn service
    * destroy: destroy a vpn service
    * add_route: add the ip of the container in iptables to increase the vpn group
    * del_route: delete the ip of the container in iptables to decrease the vpn group
    * restart_vpn: restart a vpn service
* **name**: is the container name, this can use with:
    * create
    * destroy
    * add_route
    * del_route
    * restart_vpn
* **iso2**: is the iso 2 country to use in the vpn service, this can use with:
    * create
    * restart_vpn

Example:

```bash
127.0.0.1:5151> PUBLISH vpn_channel '{"task": "create", "name": "vpn_01", "iso2": "US"}'
```

## Built With

* [Python](https://www.python.org/) - The language used
* [Redis](https://redislabs.com/) - Database used
* [Redis SDK](https://pypi.org/project/redis/) - Library to connect Redis
* [Docker SDK](https://docker-py.readthedocs.io/en/stable/) - Library to connect Redis

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/lennin.caro/erebo/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Lennin Caro** - *Initial work* - [Erebo](https://gitlab.com/lennin.caro/erebo)

See also the list of [contributors](https://gitlab.com/lennin.caro/erebo/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/lennin.caro/erebo/LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

[ISO]: https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes