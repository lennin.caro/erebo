python compile.py build
rm docker_project.c utils.c vpn_utils.c
cd build/lib.linux-x86_64-3.6
mv docker_project.cpython-36m-x86_64-linux-gnu.so docker_project.so
mv utils.cpython-36m-x86_64-linux-gnu.so utils.so
mv vpn_utils.cpython-36m-x86_64-linux-gnu.so vpn_utils.so
mv docker_project.so ../../../
mv utils.so ../../../
mv vpn_utils.so ../../../
cd ../../
rm -rf build
