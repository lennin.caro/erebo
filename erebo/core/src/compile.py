from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("utils",  ["utils.py"]),
    Extension("docker_project",  ["docker_project.py"]),
    Extension("vpn_utils",  ["vpn_utils.py"]),
]

setup(
    name = 'vpn_core',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)
