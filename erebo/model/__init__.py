#!/usr/bin/python3
# -*- coding: utf-8 -*-
# import sys

__author__ = "Lennin Caro"
__copyright__ = "Copyright 2017, The Erebo (VPN WAY) Project"
__credits__ = ["Lennin Caro"]
__license__ = "GPL"
__version__ = "1.1.23"
__maintainer__ = "Lennin Caro"
__email__ = "renjin25@gmail.com"
__status__ = "Production"

# sys.path += __path__
# =====================================================
# init
# =====================================================


def init(cont):
    '''
    This member function does nothing. It is a hook
    to be called by the BGE to trigger the initialization.
    '''
    pass
